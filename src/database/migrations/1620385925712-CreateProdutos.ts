import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateProdutos1620385925712 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF006",
                columns:[
                    {
                        name:"PRO_COD",
                        type:"int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name:"PRO_NOME",
                        type:"varchar"
                    },
                    {
                        name:"PRO_REF",
                        type:"varchar"
                    },
                    {
                        name:"PRO_BARRA",
                        type:"varchar"
                    },
                    {
                        name:"PRO_SIM",
                        type:"int"
                    },
                    {
                        name:"GRU_COD",
                        type:"int"
                    },
                    {
                        name:"SUB_COD",
                        type:"int"
                    },
                    {
                        name:"PRO_ST",
                        type:"varchar"
                    },
                    {
                        name:"PRO_UNI",
                        type:"varchar"
                    },
                    {
                        name:"PRO_EMB",
                        type:"varchar"
                    },
                    {
                        name:"PRO_LOC",
                        type:"varchar"
                    },
                    {
                        name:"PRO_APL",
                        type:"varchar"
                    },
                    {
                        name:"PRO_UNIT",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COM",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CUSTO",
                        type:"double precision"
                    },
                    {
                        name:"PRO_MEDIO",
                        type:"double precision"
                    },
                    {
                        name:"PRO_LUCRO",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VENDA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_MIN",
                        type:"double precision"
                    },
                    {
                        name:"PRO_MAX",
                        type:"double precision"
                    },
                    {
                        name:"PRO_EST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar"
                    },
                    {
                        name:"PRO_FOTO",
                        type:"text"
                    },
                    {
                        name:"PRO_CF",
                        type:"varchar"
                    },
                    {
                        name:"PRO_ICMS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PESO",
                        type:"varchar"
                    },
                    {
                        name:"PRO_IPI",
                        type:"double precision"
                    },
                    {
                        name:"PRO_MAT",
                        type:"varchar"
                    },
                    {
                        name:"PRO_ICMSC",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBSL",
                        type:"double precision"
                    },
                    {
                        name:"PRO_FRETE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_QTPROD",
                        type:"double precision"
                    },
                    {
                        name:"PRO_INDUS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VALID",
                        type:"varchar"
                    },
                    {
                        name:"PRO_COMP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PESOB",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CUBO",
                        type:"varchar"
                    },
                    {
                        name:"PRO_LARG",
                        type:"int"
                    },
                    {
                        name:"PRO_ALTU",
                        type:"int"
                    },
                    {
                        name:"PRO_RED",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SIMPLES",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VEIC",
                        type:"varchar"
                    },
                    {
                        name:"PRO_CUSTOADMC",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PIS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PIS2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINS2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_RENDA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SOCIAL",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CUSTOADMV",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ICMSMVA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBS2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_IPI2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ANTPARC",
                        type:"double precision"
                    },
                    {
                        name:"PRO_DESCANT",
                        type:"double precision"
                    },
                    {
                        name:"PRO_NFECOMP",
                        type:"text"
                    },
                    {
                        name:"PRO_DESCMAX",
                        type:"text"
                    },
                    {
                        name:"PRO_GTIN",
                        type:"varchar"
                    },
                    {
                        name:"PRO_IAT",
                        type:"varchar"
                    },
                    {
                        name:"PRO_GEN",
                        type:"varchar"
                    },
                    {
                        name:"PRO_EX_IPI",
                        type:"varchar"
                    },
                    {
                        name:"PRO_TIPO",
                        type:"varchar"
                    },
                    {
                        name:"PRO_DESAT",
                        type:"varchar",
                    },
                    {
                        name:"PRO_CONVER",
                        type:"double precision"
                    },
                    {
                        name:"PRO_STIPI",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_STPIS",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_STCOFINS",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_CESP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CLAR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CCOM",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CSO",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_CREDSN",
                        type:"double precision"
                    },
                    {
                        name:"PRO_REDV",
                        type:"double precision"
                    },
                    {
                        name:"PRO_STIPI2",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_STCOFINS2",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"MD5",
                        type:"varchar",
                        width:32
                    },
                    {
                        name:"PRO_PISVL",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINSVL",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PISVL2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINSVL2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_NAT_BC_CRED",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_NAT_REC",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_PAUTA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTAALIQ",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTAMAIOR",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"SER_COD_LST",
                        type:"int"
                    },
                    {
                        name:"PRO_PRMAX",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ANP",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"CFOP_CODSN",
                        type:"varchar",
                        width:5
                    },
                    {
                        name:"CFOP_CODSD",
                        type:"varchar",
                        width:5
                    },
                    {
                        name:"CFOP_CODED",
                        type:"varchar",
                        width:5
                    },
                    {
                        name:"CFOP_CODEN",
                        type:"varchar",
                        width:5
                    },
                    {
                        name:"PRO_SUBSICMSCREDE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBSICMSCREDS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBSICMSDEBE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBICMSDEBE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SUBSMVAE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_MODBCST",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_NMOTOR",
                        type:"varchar",
                        width:21
                    },
                    {
                        name:"PRO_CMKG",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_DIST",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_RENAVAM",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_ANOMOD",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_PAUTAE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTAALIQE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTAMAIORE",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"CCON_COD",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_PESOL",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CEST",
                        type:"varchar",
                        width:7
                    },
                    {
                        name:"PRO_ICMSDIF",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ENQIPI",
                        type:"int"
                    },
                    {
                        name:"PRO_REDST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_REDVST",
                        type:"double precision"
                    },
                    {
                        name:"FAM_COD",
                        type:"int"
                    },
                    {
                        name:"SFAM_COD",
                        type:"int"
                    },
                    {
                        name:"COD_CTA_ENT",
                        type:"varchar"
                    },
                    {
                        name:"COD_CTA_SAI",
                        type:"varchar"
                    },
                    {
                        name:"PRO_PGLP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PGNN",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PGNI",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VPART",
                        type:"double precision"
                    },
                    {
                        name:"PRO_FCP",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_CONVENT",
                        type:"double precision"
                    },
                    {
                        name:"PRO_FRACIONADO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_ACOU",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_UTRIB",
                        type:"varchar",
                        width:26
                    },
                    {
                        name:"PRO_QTRIB",
                        type:"double precision"
                    }
                ]
            }), true
        ),
        await queryRunner.createIndex("DSIAF006", new TableIndex({
                name: "DSIAF006_IDX1",
                columnNames: ["PRO_COD"]
        })),
        await queryRunner.createIndex("DSIAF006", new TableIndex({
                name: "DSIAF006_IDX2",
                columnNames: ["PRO_NOME"]
        })),
        await queryRunner.createIndex("DSIAF006", new TableIndex({
                name: "DSIAF006_IDX3",
                columnNames: ["PRO_REF"]
        })),
        await queryRunner.createIndex("DSIAF006", new TableIndex({
                name: "DSIAF006_IDX4",
                columnNames: ["PRO_BARRA"]
        })),
        await queryRunner.createIndex("DSIAF006", new TableIndex({
                name: "DSIAF006_IDX5",
                columnNames: ["PRO_COD"]
        })),
        await queryRunner.createIndex("DSIAF006", new TableIndex({
                name: "DSIAF006_IDX_MD5",
                columnNames: ["MD5"]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('DSIAF006');
    }

}
