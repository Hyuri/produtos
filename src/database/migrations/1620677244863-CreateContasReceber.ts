import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateContasReceber1620677244863 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF016",
                columns:[
                    {
                        name:"PAG_NUM",
                        type:"int"
                    },
                    {
                        name:"PAG_TIPO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"FOR_COD",
                        type:"int"
                    },
                    {
                        name:"PAG_DUP",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"CEN_COD",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"PAG_HIST",
                        type:"varchar",
                        width:50
                    },
                    {
                        name:"PAG_LANC",
                        type:"timestamp"
                    },
                    {
                        name:"PAG_VENC",
                        type:"timestamp"
                    },
                    {
                        name:"PAG_DESC",
                        type:"double precision"
                    },
                    {
                        name:"PAG_VAL",
                        type:"double precision"
                    },
                    {
                        name:"PAG_DPAG",
                        type:"timestamp"
                    },
                    {
                        name:"PAG_PAG",
                        type:"double precision"
                    },
                    {
                        name:"SAI_SER",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"ENT_NOTA",
                        type:"int"
                    },
                    {
                        name:"ENT_FOR",
                        type:"int"
                    },
                    {
                        name:"PAG_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"SELECAO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"CAI_COD",
                        type:"int"
                    },
                    {
                        name:"PAG_JUROS",
                        type:"double precision"
                    },
                    {
                        name:"PRA_COD",
                        type:"varchar",
                        width:19
                    },
                    {
                        name:"PAG_BRUTO",
                        type:"double precision"
                    },
                    {
                        name:"PAG_PARC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PAG_TGERA",
                        type:"int"
                    },
                    {
                        name:"DEP_COD",
                        type:"int"
                    },
                    {
                        name:"PAG_COD_OR",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PAG_COD_REC",
                        type:"varchar",
                        width:30
                    },
                    {
                        name:"PAG_NUM_PROC",
                        type:"varchar",
                        width:15
                    },
                    {
                        name:"PAG_IND_PROC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PAG_TXT_COMPL",
                        type:"varchar",
                        width:100
                    },
                    {
                        name:"PAG_MES_REF",
                        type:"varchar",
                        width:7
                    },
                    {
                        name:"PAG_PROC",
                        type:"varchar",
                        width:100
                    },
                    {
                        name:"PRA_CODS",
                        type:"int"
                    },
                    {
                        name:"TIP_CODS",
                        type:"int"
                    },
                    {
                        name:"PAG_OBSBAIXA",
                        type:"varchar",
                        width:20
                    }
                ]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX1",
                columnNames: ["PAG_NUM"]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX2",
                columnNames: ["PAG_DUP","PAG_TIPO","FOR_COD","PAG_VENC"]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX3",
                columnNames: ["PAG_DPAG"]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX4",
                columnNames: ["ENT_NOTA","ENT_FOR"]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX5",
                columnNames: ["SAI_SER","ENT_NOTA"]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX6",
                columnNames: ["PAG_DPAG","CEN_COD"]
            })),
            await queryRunner.createIndex("DSIAF016",  new TableIndex({
                name: "DSIAF016_IDX7",
                columnNames: ["PAG_NUM"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF016");
    }

}
