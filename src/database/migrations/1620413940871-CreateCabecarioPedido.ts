import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateCabecarioPedido1620413940871 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF056",
                columns:[
                    {
                        name:"LIS_SAI_PED",
                        type:"int"
                    },
                    {
                        name:"SAI_DATA",
                        type:"timestamp"
                    },
                    {
                        name:"CLI_COD",
                        type:"int"
                    },
                    {
                        name:"CLI_NOME",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"CLI_END",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"CLI_BAI",
                        type:"varchar",
                        width:30
                    },
                    {
                        name:"CLI_CEP",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"CLI_CID",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"CLI_EST",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"CLI_FONE",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"CLI_CPF",
                        type:"varchar",
                        width:14
                    },
                    {
                        name:"CLI_CGC",
                        type:"varchar",
                        width:18
                    },
                    {
                        name:"CLI_INSC",
                        type:"varchar",
                        width:18
                    },
                    {
                        name:"VEN_COD",
                        type:"int"
                    },
                    {
                        name:"SAI_CONTA",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRA_COD",
                        type:"int"
                    },
                    {
                        name:"TIP_COD",
                        type:"int"
                    },
                    {
                        name:"SAI_ACRE",
                        type:"double precision"
                    },
                    {
                        name:"SAI_DESC",
                        type:"double precision"
                    },
                    {
                        name:"SAI_TOTAL",
                        type:"double precision"
                    },
                    {
                        name:"TIPO_PAG",
                        type:"varchar",
                        width:19
                    },
                    {
                        name:"SAI_PREST",
                        type:"int"
                    },
                    {
                        name:"SAI_ENTRA",
                        type:"double precision"
                    },
                    {
                        name:"SAI_DTENT",
                        type:"timestamp"
                    },
                    {
                        name:"TRA_COD",
                        type:"int"
                    },
                    {
                        name:"TRA_FRETE",
                        type:"double precision"
                    },
                    {
                        name:"TRA_VENC",
                        type:"timestamp"
                    },
                    {
                        name:"SAI_CANCEL",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"SAI_IMP",
                        type:"varchar",
                        width:19
                    },
                    {
                        name:"SAI_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"SAI_HORA",
                        type:"timestamp"
                    },
                    {
                        name:"SAI_ENTRE",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"SAI_PREVIDT",
                        type:"timestamp"
                    },
                    {
                        name:"PRAZ_DESC",
                        type:"varchar",
                        width:30
                    },
                    {
                        name:"PED_PEND",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PED_PENDF",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PROF_COD",
                        type:"int"
                    },
                    {
                        name:"SAI_PREVENDA",
                        type:"int"
                    },
                    {
                        name:"CHAVE",
                        type:"varchar",
                        width:50
                    },
                    {
                        name:"CLI_NUM",
                        type:"int"
                    },
                    {
                        name:"SAI_OBS",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"SAI_RETI",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"CUP_CCF",
                        type:"int"
                    },
                    {
                        name:"CUP_COO",
                        type:"int"
                    },
                    {
                        name:"SAI_PEDMOB",
                        type:"int"
                    },
                    {
                        name:"CID_COD",
                        type:"int"
                    },
                    {
                        name:"ECF_NUM",
                        type:"smallint"
                    },
                    {
                        name:"CUP_SERIE",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"CUP_MFADIC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"CUP_MARCAECF",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"CUP_MODELO",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"MD5",
                        type:"varchar",
                        width:32
                    },
                    {
                        name:"PED_PESO",
                        type:"double precision"
                    },
                    {
                        name:"SAI_MOTCANC",
                        type:"varchar",
                        width:100
                    },
                    {
                        name:"SAI_PEDMOBCHAVE",
                        type:"varchar",
                        width:36
                    },
                    {
                        name:"PRO_ST",
                        type:"varchar",
                        width:3
                    }
                ]   
            })),

            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF056_IDX_MD5",
                columnNames: ["CLI_COD","SAI_DATA"]
            })),
            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF156",
                columnNames: ["SAI_DATA","LIS_SAI_PED"]
            })),
            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF256",
                columnNames: ["SAI_DATA","LIS_SAI_PED"]
            })),
            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF456",
                columnNames: ["VEN_COD","SAI_DATA"]
            })),
            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF556",
                columnNames: ["PED_PEND","LIS_SAI_PED"]
            })),
            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF656",
                columnNames: ["PED_PENDF"]
            })),
            await queryRunner.createIndex("DSIAF056",  new TableIndex({
                name: "DSIAF756",
                columnNames: ["CHAVE"]
            }))

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF056");
    }

}
