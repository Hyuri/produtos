import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateSubgrupoProdutos1620391783919 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF019",
                columns:[
                    {
                        name:"SUB_COD",
                        type:"int"
                    },
                    {
                        name:"SUB_NOME",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"SUB_CAD",
                        type:"timestamp"
                    }
                ]
            })),
            await queryRunner.createIndex("DSIAF019",  new TableIndex({
                name: "DSIAF019_IDX1",
                columnNames: ["SUB_COD"]
            })),
            await queryRunner.createIndex("DSIAF019",  new TableIndex({
                name: "DSIAF019_IDX2",
                columnNames: ["SUB_NOME"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF019")
    }

}
