import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateItensPedido1620417577268 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF057",
                columns:[
                    {
                        name:"LIS_SAI_PED",
                        type:"int"
                    },
                    {
                        name:"LIS_COD",
                        type:"int"
                    },
                    {
                        name:"PRO_COD",
                        type:"int"
                    },
                    {
                        name:"PRO_BARRA",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_NOME",
                        type:"varchar",
                        width:50
                    },
                    {
                        name:"PRO_EST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VENDA",
                        type:"double precision"
                    },
                    {
                        name:"SAI_DATA",
                        type:"timestamp"
                    },
                    {
                        name:"SAI_HORA",
                        type:"time"
                    },
                    {
                        name:"SAI_CANCEL",
                        type:"varchar"
                    },
                    {
                        name:"PRO_EST2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ENTRE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PEDIDO",
                        type:"double precision"
                    },
                    {
                        name:"SAI_ORCV",
                        type:"int"
                    },
                    {
                        name:"LIS_CODORI",
                        type:"int"
                    },
                    {
                        name:"PRO_GRADE",
                        type:"varchar"
                    },
                    {
                        name:"PRO_VENDAOLD",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VLARG",
                        type:"int"
                    },
                    {
                        name:"PRO_VALT",
                        type:"int"
                    },
                    {
                        name:"PRO_VESP",
                        type:"int"
                    },
                    {
                        name:"PRO_VCOR",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"PRO_PROJ",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_VVID",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"PRO_VCAN",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"PRO_VACA",
                        type:"varchar",
                        width:22
                    },
                    {
                        name:"PRO_VESP1",
                        type:"varchar",
                        width:17
                    },
                    {
                        name:"PRO_VESP2",
                        type:"varchar",
                        width:17
                    },
                    {
                        name:"PRO_CODP",
                        type:"int"
                    },
                    {
                        name:"PED_FECHA",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_VENDAITEM",
                        type:"double precision"
                    },
                    {
                        name:"PRO_DESCITEM",
                        type:"double precision"
                    },
                    {
                        name:"EXE_DESC",
                        type:"varchar",
                        width:200
                    },
                    {
                        name:"PRO_OBS",
                        type:"text"
                    },
                    {
                        name:"PRO_GRCOR",
                        type:"varchar",
                        width:8
                    },
                    {
                        name:"PRO_VENDAOLDT",
                        type:"double precision"
                    },
                    {
                        name:"MD5",
                        type:"varchar",
                        width:32
                    },
                    {
                        name:"PRO_VENDATAB",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ALIQ",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CCOM",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CESP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CLAR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CQUA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ST",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_UNI",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_PESOB",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PESOT",
                        type:"double precision"
                    },
                    {
                        name:"SAI_STATUSENTREGA",
                        type:"varchar",
                        width:1
                    }
                ]
            })),

            await queryRunner.createIndex("DSIAF057",  new TableIndex({
                name: "DSIAF057_IDX1",
                columnNames: ["LIS_SAI_PED"]
            })),
            await queryRunner.createIndex("DSIAF057",  new TableIndex({
                name: "DSIAF057_IDX3",
                columnNames: ["LIS_COD"]
            })),
            await queryRunner.createIndex("DSIAF057",  new TableIndex({
                name: "DSIAF057_IDX_MD5",
                columnNames: ["MD5"]
            })),
            await queryRunner.createIndex("DSIAF057",  new TableIndex({
                name: "DSIAF157",
                columnNames: ["LIS_SAI_PED", "PRO_COD"]
            })),
            await queryRunner.createIndex("DSIAF057",  new TableIndex({
                name: "DSIAF257_IDX2",
                columnNames: ["PED_FECHA"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF057");
    }

}
