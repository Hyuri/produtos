import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateFornecedores1620678046942 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF009",
                columns:[
                    {
                        name:"FOR_COD",
                        type:"int"
                    },
                    {
                        name:"FOR_RAZAO",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"FOR_NOME",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"FOR_CONT",
                        type:"varchar",
                        width:30
                    },
                    {
                        name:"FOR_END",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"FOR_BAI",
                        type:"varchar",
                        width:30
                    },
                    {
                        name:"FOR_CEP",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"FOR_CID",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"FOR_EST",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"FOR_FONE",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"FOR_FAX",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"FOR_MAIL",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"FOR_CGC",
                        type:"varchar",
                        width:18
                    },
                    {
                        name:"FOR_INSC",
                        type:"varchar",
                        width:18
                    },
                    {
                        name:"CEN_COD",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"FOR_OBS",
                        type:"text"
                    },
                    {
                        name:"FOR_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"FOR_CPF",
                        type:"varchar",
                        width:14
                    },
                    {
                        name:"FOR_TIPO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"FOR_INSCP",
                        type:"varchar",
                        width:18
                    },
                    {
                        name:"FOR_SITE",
                        type:"varchar",
                        width:50
                    },
                    {
                        name:"FOR_SUFRAMA",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"DEP_COD",
                        type:"int"
                    },
                    {
                        name:"FOR_END_NUM",
                        type:"varchar",
                        width:6
                    },
                    {
                        name:"FOR_CEL",
                        type:"varchar",
                        width:16
                    }
                ]
            })),
            await queryRunner.createIndex("DSIAF009",  new TableIndex({
                name: "DSIAF009_IDX1",
                columnNames: ["FOR_COD"]
            })),
            await queryRunner.createIndex("DSIAF009",  new TableIndex({
                name: "DSIAF009_IDX2",
                columnNames: ["FOR_RAZAO"]
            })),
            await queryRunner.createIndex("DSIAF009",  new TableIndex({
                name: "DSIAF009_IDX3",
                columnNames: ["FOR_NOME"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF009");
    }

}
