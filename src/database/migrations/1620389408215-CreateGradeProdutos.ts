import {Column, MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateGradeProdutos1620389408215 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF140",
                columns:[
                    {
                        name:"PRO_COD",
                        type:"int"
                    },
                    {
                        name:"PRO_BARRA",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_GRADE",
                        type:"varchar",
                        width:15
                    },
                    {
                        name:"PRO_ESTG",
                        type:"double precision"
                    },
                    {
                        name:"PRO_TPOP",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_CCOR",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_XCOR",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"PRO_POT",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_CM3",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_PESOL",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_PESOB",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_NMOTOR",
                        type:"varchar",
                        width:21
                    },
                    {
                        name:"PRO_CMKG",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_DIST",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_ANOMOD",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_ANOFAB",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_TPPINT",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_TPVEIC",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_ESPVEIC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_VIN",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_CONDVEIC",
                        type:"varchar",
                        width:12
                    },
                    {
                        name:"PRO_CMOD",
                        type:"varchar",
                        width:6
                    },
                    {
                        name:"LIS_COD",
                        type:"int"
                    },
                    {
                        name:"PRO_TPCOMB",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_CORDENAT",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_LOTA",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_TPREST",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_GRCOR",
                        type:"varchar",
                        width:8
                    },
                    {
                        name:"PRO_LOTFAB",
                        type:"timestamp",
                    },
                    {
                        name:"PRO_LOTVALID",
                        type:"timestamp",
                    }
                ]   
            }), true),
            await queryRunner.createIndex("DSIAF140",  new TableIndex({
                name: "DSIAF140_IDX1",
                columnNames: ["PRO_COD","PRO_BARRA","PRO_GRADE","PRO_GRCOR"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF140")
    }

}
