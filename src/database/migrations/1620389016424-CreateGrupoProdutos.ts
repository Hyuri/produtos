import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateGrupoProdutos1620389016424 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF005",
                columns:[
                    {
                        name:"GRU_COD",
                        type:"int"
                    },
                    {
                        name:"GRU_NOME",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"GRU_CAD",
                        type:"timestamp"
                    }
                ]
            })),
            await queryRunner.createIndex("DSIAF005", new TableIndex({
                name: "DSIAF005_IDX1",
                columnNames: ["GRU_COD"]
            }))
            
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('DSIAF005');
    }

}
