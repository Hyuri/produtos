import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateFuncionarios1620675220598 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF003",
                columns:[
                    {
                        name:"VEN_COD",
                        type:"int"
                    },
                    {
                        name:"VEN_NOME",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"VEN_END",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"VEN_BAI",
                        type:"varchar",
                        width:30
                    },
                    {
                        name:"VEN_CEP",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"VEN_CID",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"VEN_EST",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"VEN_FONE",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"VEN_ADM",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_CARGO",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"VEN_CI",
                        type:"varchar",
                        width:12
                    },
                    {
                        name:"VEN_CPF",
                        type:"varchar",
                        width:14
                    },
                    {
                        name:"VEN_TRAB",
                        type:"varchar",
                        width:15
                    },
                    {
                        name:"VEN_PORCT",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_PORCV",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_PORCP",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_PORCTS",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_PORCVS",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_PORCPS",
                        type:"timestamp"
                    },
                    {
                        name:"USU_COD",
                        type:"int"
                    },
                    {
                        name:"VEN_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_FOTO",
                        type:"varchar"
                    },
                    {
                        name:"VEN_DEM",
                        type:"timestamp"
                    },
                    {
                        name:"VEN_AFAS",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"DEP_COD",
                        type:"int"
                    },
                    {
                        name:"VEN_DESC",
                        type:"double precision"
                    },
                    {
                        name:"VEN_EVEND",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"VEN_OBS",
                        type:"varchar"
                    }
                ]
            })),
            await queryRunner.createIndex("DSIAF003",  new TableIndex({
                name: "DSIAF003_IDX1",
                columnNames: ["VEN_COD"]
            })),
            await queryRunner.createIndex("DSIAF003",  new TableIndex({
                name: "DSIAF003_IDX2",
                columnNames: ["VEN_NOME"]
            })),
            await queryRunner.createIndex("DSIAF003",  new TableIndex({
                name: "DSIAF003_IDX3",
                columnNames: ["USU_COD"]
            }))

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF003");
    }

}
