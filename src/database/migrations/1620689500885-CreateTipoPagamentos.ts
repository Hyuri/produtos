import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateTipoPagamentos1620689500885 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF026",
                columns:[
                {
                    name:"TIP_COD",
                    type:"int"
                },
                {
                    name:"TIP_DESC",
                    type:"varchar",
                    width:2
                },
                {
                    name:"TIP_REC",
                    type:"varchar",
                    width:1
                },
                {
                    name:"TIP_QUI",
                    type:"varchar",
                    width:1
                },
                {
                    name:"TIP_QUI2",
                    type:"varchar",
                    width:1
                },
                {
                    name:"TIP_CAD",
                    type:"timestamp"
                },
                {
                    name:"ATU_USUA",
                    type:"varchar",
                    width:8
                },
                {
                    name:"PAR_CODN",
                    type:"int",
                },
                {
                    name:"PAR_CODO",
                    type:"int",
                },
                {
                    name:"PAR_CODS",
                    type:"int",
                },
                {
                    name:"PAR_CODOR",
                    type:"int",
                },
                {
                    name:"PAR_CODR",
                    type:"int",
                },
                {
                    name:"TIP_CHE",
                    type:"varchar",
                    width:1
                },
                {
                    name:"PAR_CODP",
                    type:"int",
                },
                {
                    name:"TAB_COD",
                    type:"int",
                },
                {
                    name:"TIP_BOL",
                    type:"varchar",
                    width:1
                },
                {
                    name:"TxIP_EST",
                    type:"varchar",
                    width:1
                },
                {
                    name:"TIP_COMIS",
                    type:"varchar",
                    width:1
                },
                {
                    name:"CEN_COD",
                    type:"varchar",
                    width:1
                },
                {
                    name:"TIP_PROMIS",
                    type:"varchar",
                    width:1
                },
                {
                    name:"CAI_COD",
                    type:"int",           
                }
                ]
            })
        )
                        
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
       await queryRunner.dropTable("DSIAF026")
    }

}
