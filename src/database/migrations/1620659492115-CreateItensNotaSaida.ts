import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateItensNotaSaida1620659492115 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF037",
                columns:[
                    {
                        name:"SAI_SER",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_COD",
                        type:"int"
                    },
                    {
                        name:"PRO_BARRA",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_NOME",
                        type:"varchar",
                        width:50
                    },
                    {
                        name:"SAI_PED",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ICMS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_IPI",
                        type:"double precision"
                    },
                    {
                        name:"SAI_DATA",
                        type:"timestamp"
                    },
                    {
                        name:"LIS_COD",
                        type:"int"
                    },
                    {
                        name:"SAI_HORA",
                        type:"time"
                    },
                    {
                        name:"SAI_SE",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_DESC",
                        type:"double precision"
                    },
                    {
                        name:"SAI_CUPOM",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"SAI_CANCEL",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_EST2",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ENT",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SAI",
                        type:"double precision"
                    },
                    {
                        name:"PRO_EST",
                        type:"double precision"
                    },
                    {
                        name:"TEM_KIT",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"TIP_REC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"CFOP_COD",
                        type:"varchar",
                        width:5
                    },
                    {
                        name:"PRO_ENTRE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_GRADE",
                        type:"varchar",
                        width:15
                    },
                    {
                        name:"PRO_VENDAOLD",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ST2",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_CESP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CLAR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CCOM",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CQUA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VLARG",
                        type:"int"
                    },
                    {
                        name:"PRO_VALT",
                        type:"int"
                    },
                    {
                        name:"PRO_VESP",
                        type:"int"
                    },
                    {
                        name:"PRO_VCOR",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"SAI_ORCV",
                        type:"int"
                    },
                    {
                        name:"PRO_PROJ",
                        type:"int"
                    },
                    {
                        name:"PRO_CODP",
                        type:"int"
                    },
                    {
                        name:"CUP_PED",
                        type:"int"
                    },
                    {
                        name:"SAI_REQ",
                        type:"int"
                    },
                    {
                        name:"LIS_CODORI",
                        type:"int"
                    },
                    {
                        name:"PRO_VVID",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"PRO_VCAN",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"PRO_VACA",
                        type:"varchar",
                        width:22
                    },
                    {
                        name:"PRO_VESP1",
                        type:"varchar",
                        width:17
                    },
                    {
                        name:"PRO_VESP2",
                        type:"varchar",
                        width:17
                    },
                    {
                        name:"SAI_PEDV",
                        type:"int"
                    },
                    {
                        name:"PRO_TPOP",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_CHASSI",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRO_CCOR",
                        type:"varchar",
                        width:17
                    },
                    {
                        name:"PRO_XCOR",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_POT",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"PRO_CM3",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_PESOL",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_PESOB",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_NSERIE",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_NMOTOR",
                        type:"varchar",
                        width:21
                    },
                    {
                        name:"PRO_CMKG",
                        type:"varchar",
                        width:9
                    },
                    {
                        name:"PRO_DIST",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_ANOFAB",
                        type:"varchar",
                        width:4
                    },
                    {
                        name:"PRO_TPPINT",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_TPVEIC",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_ESPVEIC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_VIN",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_CONDVEIC",
                        type:"varchar",
                        width:12
                    },
                    {
                        name:"PRO_CMOD",
                        type:"varchar",
                        width:6
                    },
                    {
                        name:"PRO_VENDAOLDT",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VENDAITEM",
                        type:"double precision"
                    },
                    {
                        name:"PRO_DESCITEM",
                        type:"double precision"
                    },
                    {
                        name:"TIP_EST",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_TPCOMB",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_CORDENAT",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_IMPDDI",
                        type:"timestamp"
                    },
                    {
                        name:"PRO_LOTA",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_TPREST",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_IMPNDI",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_IMPXLOCDESEMB",
                        type:"varchar",
                        width:60
                    },
                    {
                        name:"PRO_IMPUFDESEMB",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_IMPDDESEMB",
                        type:"timestamp"
                    },
                    {
                        name:"PRO_IMPCEXPORTADOR",
                        type:"varchar",
                        width:60
                    },
                    {
                        name:"PRO_IMPVBC",
                        type:"double precision"
                    },
                    {
                        name:"PRO_IMPVDESPADU",
                        type:"double precision"
                    },
                    {
                        name:"PRO_IMPVII",
                        type:"double precision"
                    },
                    {
                        name:"PRO_IMPVIOF",
                        type:"double precision"
                    },
                    {
                        name:"PRO_GRCOR",
                        type:"varchar",
                        width:8
                    },
                    {
                        name:"PRO_DESONICMS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_DESONMOTIV",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_STPIS2",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_STCOFINS2",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_LOTVALID",
                        type:"timestamp"
                    },
                    {
                        name:"PRO_NAT_REC",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_STIPI2",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRO_REDV",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PICMSST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PISBASE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PISP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PISVR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_RENDAP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_RENDAVR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SEG",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINSP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_BCICMS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTAALIQ",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VICMS",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SOCIALVR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SOCIALP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SIMPLESVR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_SIMPLESP",
                        type:"double precision"
                    },
                    {
                        name:"PRO_BCSTRET",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINSVR",
                        type:"double precision"
                    },
                    {
                        name:"PRO_COFINSBASE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_BCST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_BCIPI",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CSOSN",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"PRO_FRETE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CREDSN",
                        type:"double precision"
                    },
                    {
                        name:"PRO_CREDICMSSN",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VIPI",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VICMSST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ICMSMVA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_ICMSSTRET",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTAMAIOR",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_PAUTABASE",
                        type:"double precision"
                    },
                    {
                        name:"PRO_PAUTA",
                        type:"double precision"
                    },
                    {
                        name:"PRO_OUTRO",
                        type:"double precision"
                    },
                    {
                        name:"PRO_MVAST",
                        type:"double precision"
                    },
                    {
                        name:"SAI_STATUSENTREGA",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_CF",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"IBPT",
                        type:"double precision"
                    },
                    {
                        name:"CFOP_IBPT",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRO_MODBCST",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"SAI_DENEGADO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"SAI_SERORI",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"SAI_PEDORI",
                        type:"int"
                    },
                    {
                        name:"ENT_NOTAORI",
                        type:"int"
                    },
                    {
                        name:"FOR_CODORI",
                        type:"int"
                    },
                    {
                        name:"PRO_CEST",
                        type:"varchar",
                        width:7
                    },
                    {
                        name:"PRO_VICMSUFDEST",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VICMSUFREMET",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VFCPUFDEST",
                        type:"double precision"
                    },
                    {
                        name:"IBPT_ESTADUAL",
                        type:"double precision"
                    },
                    {
                        name:"PRO_IMPDRAWBACK",
                        type:"varchar",
                        width:11
                    },
                    {
                        name:"PRO_UNI",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"VBCFCP",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"PRO_REDVST",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"PFCP",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"VFCP",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"VBCFCPST",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"PFCPST",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"VFCPST",
                        type:"double precision",
                        default:0
                    },
                    {
                        name:"PRO_UTRIB",
                        type:"varchar",
                        width:6
                    },
                    {
                        name:"PRO_QTRIB",
                        type:"double precision"
                    },
                    {
                        name:"PRO_VUNTRIB",
                        type:"varchar",
                        width:15
                    },
                    {
                        name:"PRO_NITEMPED",
                        type:"varchar",
                        width:6
                    }
                ]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF037");
    }

}
