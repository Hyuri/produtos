import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateSubfamiliaProdutos1620390973354 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF164",
                columns:[
                    {
                        name:"SFAM_COD",
                        type:"int"
                    },
                    {
                        name:"SFAM_DESC",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"SFAM_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    }
                ]        
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF164");
    }

}
