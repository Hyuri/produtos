import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateFamiliaProdutos1620389235185 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF163",
                columns:[
                    {
                        name:"FAM_COD",
                        type:"int"
                    },
                    {
                        name:"FAM_DESC",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"FAM_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:40
                    }
                ]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('DSIAF163');
    }

}
