import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateTipoVenda1620658754543 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF025",
                columns:[
                    {
                        name:"PRA_COD",
                        type:"int"
                    },
                    {
                        name:"PRA_DESC",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRA_PER",
                        type:"int"
                    },
                    {
                        name:"PRA_ENT",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRA_JUR",
                        type:"double precision"
                    },
                    {
                        name:"PRA_ECF",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"TIP_COD",
                        type:"int"
                    },
                    {
                        name:"PRA_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"PRA_TEF",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRA_ECFVIN",
                        type:"varchar",
                        width:2
                    },
                    {
                        name:"PRA_VIAS",
                        type:"int"
                    },
                    {
                        name:"PRA_CMD",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"TEF_REDE",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"PRA_TXCART",
                        type:"double precision"
                    },
                    {
                        name:"PRA_TECLA",
                        type:"varchar",
                        width:18
                    },
                    {
                        name:"PRA_TEFCD",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRA_CARTAO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"FOR_COD",
                        type:"int"
                    },
                    {
                        name:"PRA_TEFPOS",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"PRA_MINENT",
                        type:"double precision"
                    }
                ]
            }))

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF025");
    }

}
