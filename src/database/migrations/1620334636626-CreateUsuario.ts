import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateUsuario1620334636626 implements MigrationInterface {
   
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table(
               {
                name: "DSIAF050",
                columns:[
                    {
                        name:"id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name:"USU_COD",
                        type:"int",
                    },
                    {
                        name:"USU_NOME",
                        type:"varchar"
                    },
                    {
                        name:"USU_SENHA",
                        type:"varchar"
                    },
                    {
                        name:"GRU_USU",
                        type:"int"
                    },
                    {
                        name:"USU_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type: 'varchar',
                    }
                ]
               }
            )
            , true),

            await queryRunner.createIndex("DSIAF050", new TableIndex({
                name: "DSIAF050_IDX1",
                columnNames: ["USU_COD"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('DSIAF050');
    }

}
