import {MigrationInterface, QueryRunner, Table, TableIndex} from "typeorm";

export class CreateCabecarioNotaSaida1620675971708 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name:"DSIAF015",
                columns:[
                    {
                        name:"REC_DUP",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"REC_BAN",
                        type:"varchar",
                        width:20
                    },
                    {
                        name:"CLI_COD",
                        type:"int"
                    },
                    {
                        name:"CLI_NOME",
                        type:"varchar",
                        width:40
                    },
                    {
                        name:"REC_HIST",
                        type:"varchar",
                        width:50
                    },
                    {
                        name:"VEN_COD",
                        type:"int"
                    },
                    {
                        name:"REC_LANC",
                        type:"timestamp"
                    },
                    {
                        name:"PRA_COD",
                        type:"int"
                    },
                    {
                        name:"TIP_COD",
                        type:"int"
                    },
                    {
                        name:"REC_VENC",
                        type:"timestamp"
                    },
                    {
                        name:"REC_BRUTO",
                        type:"double precision"
                    },
                    {
                        name:"REC_DESC",
                        type:"double precision"
                    },
                    {
                        name:"REC_VAL",
                        type:"double precision"
                    },
                    {
                        name:"VEN_BAI",
                        type:"int"
                    },
                    {
                        name:"CEN_COD",
                        type:"varchar",
                        width:10
                    },
                    {
                        name:"REC_DPAG",
                        type:"timestamp"
                    },
                    {
                        name:"REC_DIAS",
                        type:"int"
                    },
                    {
                        name:"REC_JUROS",
                        type:"double precision"
                    },
                    {
                        name:"REC_PAG",
                        type:"double precision"
                    },
                    {
                        name:"SAI_GERA",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"SAI_SER",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"SAI_NOTA",
                        type:"int"
                    },
                    {
                        name:"CUP_PED",
                        type:"int"
                    },
                    {
                        name:"SER_SER",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"SER_NOTA",
                        type:"int"
                    },
                    {
                        name:"CUPS_PED",
                        type:"int"
                    },
                    {
                        name:"REC_CAD",
                        type:"timestamp"
                    },
                    {
                        name:"ATU_USUA",
                        type:"varchar",
                        width:80
                    },
                    {
                        name:"SELECAO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"TIPO_PAG",
                        type:"varchar",
                        width:19
                    },
                    {
                        name:"CAI_COD",
                        type:"int"
                    },
                    {
                        name:"SER_OS",
                        type:"int"
                    },
                    {
                        name:"REC_PARC",
                        type:"varchar",
                        width:16
                    },
                    {
                        name:"REC_TGERA",
                        type:"int"
                    },
                    {
                        name:"REC_FC",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"CAI_CODG",
                        type:"int"
                    },
                    {
                        name:"REC_IMPORT",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"VRTAXAGARCON",
                        type:"double precision"
                    },
                    {
                        name:"PEDIDO_GOURMET",
                        type:"int"
                    },
                    {
                        name:"CODPGTOPARCIAL",
                        type:"int"
                    },
                    {
                        name:"PGTOPARCIAL",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"REC_COMP",
                        type:"text"
                    },
                    {
                        name:"PERCTAXAGARCON",
                        type:"double precision"
                    },
                    {
                        name:"VRPGTOPARCIAL",
                        type:"double precision"
                    },
                    {
                        name:"REC_LANCH",
                        type:"timestamp"
                    },
                    {
                        name:"ARQ_BOLETO",
                        type:"varchar",
                        width:1
                    },
                    {
                        name:"REC_MULTA",
                        type:"double precision"
                    },
                    {
                        name:"CTE_SER",
                        type:"varchar",
                        width:3
                    },
                    {
                        name:"CTE_NUM",
                        type:"int"
                    }
                ]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX1",
                columnNames: ["REC_DUP"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX10",
                columnNames: ["SAI_GERA","REC_TGERA"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX2",
                columnNames: ["CLI_COD","REC_VENC"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX3",
                columnNames: ["REC_DPAG"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX4",
                columnNames: ["SAI_SER","SAI_NOTA"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX5",
                columnNames: ["REC_LANC"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX6",
                columnNames: ["CUP_PED"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX7",
                columnNames: ["SER_SER","SER_NOTA"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX8",
                columnNames: ["CUPS_PED"]
            })),
            await queryRunner.createIndex("DSIAF015",  new TableIndex({
                name: "DSIAF015_IDX9",
                columnNames: ["SER_OS"]
            }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("DSIAF015");
    }

}
