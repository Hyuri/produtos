import { Request, Response } from 'express';
import { CreateProdutoUseCase } from "./CreateProdutoUseCase";



class CreateProdutoController {

    constructor(private createProdutoUseCase:CreateProdutoUseCase){

    }

    async handle(request:Request, response:Response):Promise<Response>{
        
        const { nome, preco, descricao } = await request.body;
        
        const r = await this.createProdutoUseCase.execute({nome, preco, descricao})

        return response.status(201).json(r);

    
    }

}

export { CreateProdutoController }