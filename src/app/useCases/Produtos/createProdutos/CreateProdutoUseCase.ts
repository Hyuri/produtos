import { IProdutoRepository } from "../../../repositories/produtos/IProdutoRepository";


interface IRequest {
    nome:string,
    preco:number,
    descricao:string
}


class CreateProdutoUseCase {

    constructor(private produtoRepository:IProdutoRepository){

    }

    async execute({ nome, preco, descricao }:IRequest):Promise<any>{
         return this.produtoRepository.create({nome, preco, descricao})
    }

}

export { CreateProdutoUseCase }