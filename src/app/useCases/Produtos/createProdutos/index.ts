import { ProdutoRepository } from "../../../repositories/produtos/implements/ProdutoRepository";
import { CreateProdutoController } from "./CreateProdutoController";
import { CreateProdutoUseCase } from "./CreateProdutoUseCase";
;


export default(): CreateProdutoController => {

const produtoRepository = new ProdutoRepository();
const createProdutoUseCase = new CreateProdutoUseCase(produtoRepository);
const createProdutoController = new CreateProdutoController(createProdutoUseCase);

return createProdutoController

}
