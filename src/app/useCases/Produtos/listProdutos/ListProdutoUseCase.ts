import { Produto } from "../../../entities/Produto"
import { IProdutoRepository } from "../../../repositories/produtos/IProdutoRepository"



class ListProdutoUseCase {

    constructor(private produtoRepository:IProdutoRepository){

    }

    async execute(nome=null, pagina, limit):Promise<any>{

      const total_paginas = this.produtoRepository.count(nome);


      const dados = await this.produtoRepository.list(nome, parseInt(limit), parseInt(pagina));
      const pag_anterior = await parseInt(pagina) - 1;
      const pag_atual = await parseInt(pagina);
      const pag_posterior = await (parseInt(total_paginas) == parseInt(pagina)) ? (parseInt(pagina) + 1) : null;
      const paginas = await total_paginas;
    
      return {
        dados,
        paginacao:{
          pag_anterior,
          pag_atual,
          pag_posterior,
          paginas
        }
      }
      

    }

}

export { ListProdutoUseCase }