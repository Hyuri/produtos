import { Request, Response } from 'express';
import { ListProdutoUseCase } from './ListProdutoUseCase';


class ListProdutoController {

    constructor(private listProdutoUseCase:ListProdutoUseCase){
        
    }

    async handle(request:Request, response:Response):Promise<Response>{
          const { nome } = await request.body;
          const  limit = +request.query.limit || 10;
          const  pagina = +request.query.page || 0;
          const all = await this.listProdutoUseCase.execute(nome, pagina, limit);
          return response.status(201).json(all);
    }

}

export { ListProdutoController }