import { ProdutoRepository } from "../../../repositories/produtos/implements/ProdutoRepository";
import { ListProdutoController } from "./ListProdutoController";
import { ListProdutoUseCase } from "./ListProdutoUseCase";



export default(): ListProdutoController => {

const produtoRepository = new ProdutoRepository();
const listProdutoUseCase = new ListProdutoUseCase(produtoRepository);
const listProdutoController = new ListProdutoController(listProdutoUseCase);

return listProdutoController

}
