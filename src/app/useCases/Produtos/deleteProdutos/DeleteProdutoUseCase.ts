import { response } from "express";
import { IProdutoRepository } from "../../../repositories/produtos/IProdutoRepository";



class DeleteProdutoUseCase {

    constructor(private produtoRepository:IProdutoRepository) {

    }

    async execute(id):Promise<any>{
       return await this.produtoRepository.delete(id);
    }

}

export { DeleteProdutoUseCase }