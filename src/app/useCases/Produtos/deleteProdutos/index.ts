import { ProdutoRepository } from "../../../repositories/produtos/implements/ProdutoRepository";
import { DeleteProdutoController } from "./DeleteProdutoController";
import { DeleteProdutoUseCase } from "./DeleteProdutoUseCase";


export default(): DeleteProdutoController => {

const produtoRepository = new ProdutoRepository();
const deleteProdutoUseCase = new DeleteProdutoUseCase(produtoRepository);
const deleteProdutoController = new DeleteProdutoController(deleteProdutoUseCase);

return deleteProdutoController

}
