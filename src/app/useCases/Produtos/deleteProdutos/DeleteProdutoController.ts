import { Request, Response } from 'express';
import { DeleteProdutoUseCase } from './DeleteProdutoUseCase';

class DeleteProdutoController {

    constructor(private deleteProdutoUseCase:DeleteProdutoUseCase) {

    }

    async handle(request:Request, response:Response):Promise<Response>{
        
        const { id } = await request.body;

        const r = await this.deleteProdutoUseCase.execute(id)

        return response.status(201).json(r)
    
    }

}

export { DeleteProdutoController }