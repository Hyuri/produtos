import { IProdutoRepository } from "../../../repositories/produtos/IProdutoRepository";


interface IRequest {
    id:number,
    nome:string,
    preco:number,
    descricao:string
}


class UpdateProdutoUseCase {

    constructor(private produtoRepository:IProdutoRepository){
           
    }

    async execute({ id, nome, preco, descricao }:IRequest):Promise<any>{
        return await this.produtoRepository.update({id , nome ,preco, descricao})
    }

}

export { UpdateProdutoUseCase }