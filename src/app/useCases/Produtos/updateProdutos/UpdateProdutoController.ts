import { Request, Response } from 'express'
import { UpdateProdutoUseCase } from "./UpdateProdutoUseCase";


class UpdateProdutoController {

    constructor(private updateProdutoUseCase:UpdateProdutoUseCase){

    }

    async handle(request:Request, response:Response):Promise<Response>{
        
        const { id, nome, preco, descricao } = await request.body;

        const r =  await this.updateProdutoUseCase.execute({ id, nome, preco, descricao });

        return response.status(201).json(r);

    }

}

export { UpdateProdutoController }