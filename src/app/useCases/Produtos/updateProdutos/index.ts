import { ProdutoRepository } from "../../../repositories/produtos/implements/ProdutoRepository";
import { UpdateProdutoController } from "./UpdateProdutoController";
import { UpdateProdutoUseCase } from "./UpdateProdutoUseCase";



export default(): UpdateProdutoController => {

const produtoRepository = new ProdutoRepository();
const updateProdutoUseCase = new UpdateProdutoUseCase(produtoRepository);
const updateProdutoController = new UpdateProdutoController(updateProdutoUseCase);

return updateProdutoController

}
