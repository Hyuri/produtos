import { ProdutoRepository } from "../../../repositories/produtos/implements/ProdutoRepository";
import { UpdateProdutoController } from "../updateProdutos/UpdateProdutoController";
import { UpdateProdutoUseCase } from "../updateProdutos/UpdateProdutoUseCase";
import { EditProdutoController } from "./EditProdutoController";
import { EditProdutoUseCase } from "./editProdutoUseCase";

export default(): EditProdutoController => {

const produtoRepository = new ProdutoRepository();
const editProdutoUseCase = new EditProdutoUseCase(produtoRepository);
const editProdutoController = new EditProdutoController(editProdutoUseCase);

return editProdutoController

}
