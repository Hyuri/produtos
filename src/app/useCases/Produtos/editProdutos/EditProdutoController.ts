import { Request, Response } from 'express';
import { EditProdutoUseCase } from "./editProdutoUseCase";



class EditProdutoController {

    constructor(private editProdutoUseCase:EditProdutoUseCase){

    }

    async handle(request:Request, response:Response):Promise<Response>{
        const { id } = await request.body;
        const all = await this.editProdutoUseCase.execute(id);
        return response.status(201).json(all);
    }

}

export { EditProdutoController }