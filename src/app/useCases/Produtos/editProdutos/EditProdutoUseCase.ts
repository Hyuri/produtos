import { Produto } from "../../../entities/Produto";
import { IProdutoRepository } from "../../../repositories/produtos/IProdutoRepository";



class EditProdutoUseCase {

    constructor(private produtoRepository:IProdutoRepository){

    }

    async execute(id):Promise<Produto>{
           return this.produtoRepository.findById(id);    
    }

}

export { EditProdutoUseCase }