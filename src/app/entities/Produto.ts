import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("produtos")
class Produto {

    @PrimaryGeneratedColumn('increment')
    id:number
    
    @Column()
    nome:string
    
    @Column()
    preco:number

    @Column()
    descricao:string

    @Column()
    create_at:Date

    @Column()
    update_at:Date

    @Column()
    status:number
  


}
export { Produto }
