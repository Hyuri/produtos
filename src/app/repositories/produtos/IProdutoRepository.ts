
import { Produto } from "../../entities/Produto";
/**
 * Interface dos metodos e atributos usados no repository 
 */

interface IProdutoRepositoryDTO{
    id?:number,
    nome:string,
    preco:number,
    descricao:string
}

interface IProdutoRepository {
     list(nome:string, limit:number, pagina:number):Promise<Produto[]>
     create({nome, preco, descricao}:IProdutoRepositoryDTO):Promise<void>;
     update({id,nome, preco, descricao}:IProdutoRepositoryDTO):Promise<void>;
     findById(id:number):Promise<Produto>;
     findByName(name:string):Promise<Produto>;
     delete(id:number):Promise<any>;
     count(name:string):Promise<any>
}

export { IProdutoRepository, IProdutoRepositoryDTO }