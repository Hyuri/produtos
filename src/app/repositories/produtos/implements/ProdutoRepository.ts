import { getRepository, Repository } from "typeorm";
import { Produto } from "../../../entities/Produto";
import { IProdutoRepository, IProdutoRepositoryDTO } from "../IProdutoRepository";


class ProdutoRepository implements IProdutoRepository {

    private produtos:Repository<Produto>;

    constructor(){
        this.produtos = getRepository(Produto);
    }

    async list(nome = null, limit, pagina): Promise<Produto[]> {
        const db = await this.produtos.createQueryBuilder()
       
        if(nome){
         db.where(`nome like '%${nome}%'`)
        }

        db.offset(pagina*10)
        db.limit(limit)
        
        return db.execute();
    }

    async create({ nome, preco, descricao }: IProdutoRepositoryDTO): Promise<void> {
      const produto = await this.produtos.create({
            nome,
            preco,
            descricao,
            status:1,
            create_at:Date()
          });

      this.produtos.save(produto);
    }

    async update({ id, nome, preco, descricao }: IProdutoRepositoryDTO): Promise<any> {
        
     
        let produto = await this.produtos.findOne(id);

        produto.nome = nome;
        produto.preco = preco;
        produto.descricao = descricao;
        produto.status = 1;

        await this.produtos.save(produto);
     
    }

    async findById(id: number): Promise<Produto> {
         const produto = await this.produtos.findOne(id)
         return produto;
    }

    async findByName(name: string): Promise<Produto> {
        const produto = await this.produtos.findOne(name);
        return produto;
    }

    async  delete(id: number): Promise<any> {
        const produto = await this.produtos.delete(id);

        return produto;
    }

    async count(nome = null): Promise<any> {
        const db = await this.produtos.createQueryBuilder()
        
        if(nome){
            db.where(`nome = ${nome}`)
        }
        
        return db.getCount();
    }

}

export { ProdutoRepository }