import { Router } from 'express';

import listProdutoController from '../app/useCases/Produtos/listProdutos';
import updateProdutoController from '../app/useCases/Produtos/updateProdutos';
import deleteProdutoController from '../app/useCases/Produtos/deleteProdutos';
import editProdutoController from '../app/useCases/Produtos/editProdutos';
import createProdutoController from '../app/useCases/Produtos/createProdutos';

import { ensureAuthenticated } from '../database/middleware/ensureAuthenticated';

const produtosRoutes = Router();


//financeiroRoutes.use(ensureAuthenticated);

produtosRoutes.get("/all", (request, response) => {
    return listProdutoController().handle(request, response);
})


produtosRoutes.put("/update", (request, response) => {
  return updateProdutoController().handle(request, response);
})


produtosRoutes.get("/delete", (request, response) => {
  return deleteProdutoController().handle(request, response);
})


produtosRoutes.get("/edit", (request, response) => {
  return editProdutoController().handle(request, response);
})


produtosRoutes.delete("/create", (request, response) => {
  return createProdutoController().handle(request, response);
})




export { produtosRoutes }