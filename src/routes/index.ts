
import { Router } from 'express';
import { authRoutes } from './auth.routes';
import { produtosRoutes } from './produtos.routes';
import { usuarioRoutes } from './usuario.routes';

const router = Router();

router.use("/usuarios", usuarioRoutes);
router.use("/login", authRoutes);
router.use('/produtos', produtosRoutes);



export { router }